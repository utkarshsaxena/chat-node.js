//Client side code
$(function() {
var host = '{{config.host}}';
var messages = io.connect('/messages');
var roomNum = {{room_number}};
var userName = '{{user.fullname}}';
var userPic = '{{user.profilePic}}';

messages.on('connect', function() {
console.log('messages connection established');
messages.emit('joinroom', {room: roomNum, user: userName, userPic: userPic}) 
  })

$(document).on('keyup', '.newmessage', function(e) {
    if(e.which === 13 && $(this).val() != ''){
        messages.emit('newMessage', {
            room_number:roomNum,
            user: userName,
            userPic: userPic,
            message: $(this).val()
        })
        updateMessageFeed(userPic, $(this).val());
        $(this).val('');
    }
})

messages.on('messagefeed', function(data){
    var msgs = JSON.parse(data);
    updateMessageFeed(msgs.userPic, msgs.message);

})

function updateMessageFeed(userPic, message) {

    var str = '<li>';
        str += '<div class="msgbox">';
        str += '<div class="pic"><img src="' + userPic + '"><div class="msg"><p>'+ message + '</p></div></div>';
        str += '</div>';
        str += '</li>';
    $(str).hide().prependTo($('.messages')).slideDown(100);
}

messages.on('updateUsersList', function(data) {
    var userlist = JSON.parse(data);
    $('.users').html('');
    for (var  n = 0 ; n < userlist.length; n++) {
        var str = '<li>';
        str += '<img src="' + userlist[n].userPic + '" />';
        str += '<h5>' + userlist[n].user + '</h5>';
        str += '</li>';
        $(str).prependTo($('.users')); 
    }
})

// Check if new users have joined
setInterval(function(){
    messages.emit('updateList', {room:roomNum});
}, 5000);

})