# README #

## Introduction:  

My first node.js application using express.js, node.js and socket.io. This is a basic chat application where the user can create chat-rooms. It requires Facebook authentication to use the application.
This application is deployed on Heroku. Please visit [My Chat App.](http://chatcatutk.herokuapp.com/) to view the application.

## Technologies: ##

* HTML
* CSS
* Node.js, Express.js
* Socket.io
* Heroku

## Purpose: ##

* Learn and apply NodeJs framework to create an application.
* Integrate the FacebookAPI to authenticate the user.
* A side project to improve my programming skills

## Procedure: ##

1. Browse to **[Chat App](http://chatcatutk.herokuapp.com/)**



## Timestamp: ##

** February 2015 – March 2015